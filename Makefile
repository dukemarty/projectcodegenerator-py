# Makefile (this is -*- Makefile -*-)
# 
# This is a makefile skeleton. In a lot of projects, this doesn't have
# to be modified at all. If you do have to or want to modify it,
# please let the comments guide you.
# 
#    Last Author: Martin Loesch (<martin.loesch@@kit.edu>)
#    Date of last change: 14.11.12
#
#    Author:    Martin Loesch (<loesch@@ira.uka.de>)
#    Date:      2012-08-30
#    Copyright: Martin Loesch, Chair Prof. Dillmann (HIS)
#               Institute for Anthropomatics (IFA)
#	        Karlsruhe Institute of Technology (KIT). All rights reserved
#	        http://his.anthropomatik.kit.edu

doc: 
	@doxygen

clean: mrproper

mrproper:
	@echo "Removing *~, *.pyc ..."
	@find . -name "*~" -exec rm {} \;
	@find . -name "*.pyc" -exec rm {} \;

realclean: mrproper
	@echo "Removing doc/ ..."
	@rm -rf doc/

