#! /opt/macports2/bin/python
# This is -*- Python -*- from nbg -*- coding: latin1 -*-
#
##   \file  ProjectCodeDatabaseManager.py
#    \brief Please put your documentation for this file here.
#
#    \par Last Author: Martin Loesch (<martin.loesch@@kit.edu>)
#    \par Date of last change: 14.11.12
#
#    \author   Martin Loesch (professional@@martinloesch.net)
#    \date     2012-11-14
#    \par Copyright:
#              Martin Loesch\n
#	       All rights reserved\n
#	       http://YOURHOMEPAGE
#

import sys, argparse
from ProjectCodeDatabaseQueries import *


# =============================================================================
#  Pretty print functions for project code (database) data
        
def printCategoryList(cats):
    maxLength_name = max(cats.getColumnMaxLength("name"), len("Name"))
    maxLength_code1 = max(cats.getColumnMaxLength("code1"), len("Code 1"))
    maxLength_code2 = max(cats.getColumnMaxLength("code2"), len("Code 2"))

    formatString = "   {:<" + str(maxLength_name) + "} | {:^" + str(maxLength_code1) + "} | {:^" + str(maxLength_code2) + "}"

    print formatString.format("Name", "Code 1", "Code 2")
    print "-" * (maxLength_name + maxLength_code1 + maxLength_code2 + 12)
    for c in cats:
        print formatString.format(c.name, c.code1, c.code2)
    print ""

def printProjectList(projects):
    maxLength_name = max(projects.getColumnMaxLength("name"), len("Name"))
    maxLength_date = max(projects.getColumnMaxLength("date"), len("Date"))
    maxLength_catname = max(projects.getColumnMaxLength("categoryname"), len("Category"))
    maxLength_code = max(projects.getColumnMaxLength("projectcode"), len("Projectcode"))
    
    formatString = "   {:<" + str(maxLength_name) + "} | {:" + str(maxLength_date) + "} | {:^" + str(maxLength_catname) + "} | {:^" + str(maxLength_code) + "}"
    
    print formatString.format("Name", "Date", "Category", "Projectcode")
    print "-" * (maxLength_name + maxLength_date + maxLength_catname + maxLength_code + 16)
    for p in projects:
        print formatString.format(p.name, p.date, p.categoryname, p.projectcode)
    print ""
        
    
## TRUE MAIN PROGRAM
#
## \cond false

def handleListCommand(args, dbhandler):
    lowerType = args.type.lower()
    if (lowerType == "categories")  or (lowerType == "cat"):
        cats = dbhandler.getAllCategories()
        printCategoryList(cats)
    elif (lowerType == "projects") or (lowerType == "proj"):
        projects = dbhandler.getAllProjects()
        printProjectList(projects)
    elif lowerType == "all":
        cats = dbhandler.getAllCategories()
        printCategoryList(cats)
        print("")
        projects = dbhandler.getAllProjects()
        printProjectList(projects)
    else:
        print "Unknown parameter for list command: ", args.type
        

def handleFindCommand(args, dbhandler):
    if args.type == "":
        cats = dbhandler.getCategoryByPart(args.query)
        projs = dbhandler.getProjectByPart(args.query)
        if cats:
            printCategoryList(cats)
        if projs:
            printProjectList(projs)
    else:
        lowerType = args.type.lower()
        if (lowerType == "category") or (lowerType == "cat"):
            cats = dbhandler.getCategoryByPart(args.query)
            printCategoryList(cats)
        elif (lowerType == "project") or (lowerType == "proj"):
            projs = dbhandler.getProjectByPart(args.query)
            printProjectList(projs)
            

    
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Query project code database.")
    parser.add_argument('-db', '--database', metavar='DB', required=True, help="Project code database file")
    subparsers = parser.add_subparsers(help='main commands for managing the database')

    parser_list = subparsers.add_parser('list', help="List elements from database")
    parser_list.set_defaults(func=handleListCommand)
    parser_list.add_argument('type', metavar='TYPE', choices=['project', 'proj', 'category', 'cat'], help="Type of database elements which shall be listed [project/proj, category/cat]")

    parser_find = subparsers.add_parser('find', help="find specific element(s) in database")
    parser_find.set_defaults(func=handleFindCommand)
    parser_find.add_argument('query', metavar='QUERY', help="Queried term")
    parser_find.add_argument('type', metavar='TYPE', nargs='?', default="", help="Type of elements on which is searched")
    
    args = parser.parse_args()
    
    sys.stderr.write(" ---------------------------------------------------------\n")
    sys.stderr.write(" *             ProjectCode Database Manager              *\n")
    sys.stderr.write(" ---------------------------------------------------------\n\n")

    dbhandler = DatabaseQueryHandler(args.database)

    args.func(args, dbhandler)

        
## \endcond
