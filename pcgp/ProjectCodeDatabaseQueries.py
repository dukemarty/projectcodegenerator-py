#! /opt/macports2/bin/python
# This is -*- Python -*- from nbg -*- coding: latin1 -*-
#
##   \file  ProjectCodeDatabaseQueries.py
#    \brief Please put your documentation for this file here.
#
#    \par Last Author: Martin Loesch (<martin.loesch@@kit.edu>)
#    \par Date of last change: 14.11.12
#
#    \author   Martin Loesch (professional@@martinloesch.net)
#    \date     2012-09-12
#    \par Copyright:
#              Martin Loesch\n
#	       All rights reserved\n
#              \n
#	       http://re.ve/martin-loesch\n
#	       http://sourceforge.net/users/dukemarty
#

from CodePersistencyHandler import *


CATEGORY_QUERY_ATTRIBUTES = "name, code1, code2"
PROJECT_QUERY_ATTRIBUTES = "projects.name, description, date, categoryid, categories.name, projectcode, fullcode"


# =============================================================================
## \class SqlQueryResultListWrapper
#  \brief Wrap SQL query results to allow the implementation of some convenience functions.
class SqlQueryResultListWrapper(object):
    """Wrap SQL query results to allow the implementation of some convenience functions."""
    
    def __init__(self, resultList):
        self.list = resultList

    def __len__(self):
        return len(self.list)

    def __iter__(self):
        return SqlQueryResultListWrapperIterator(self)
    
    def __str__(self):
        return str(self.list)

    def isEmpty(self):
        return len(self.list) == 0
    
    def isUnique(self):
        return len(self.list) == 1

    def getColumnMaxLengthByIndex(self, colIndex):
        res = 0
        for l in self.list:
            if len(l[colIndex]) > res:
                res = len(l[colIndex])
        return res


# =============================================================================
## \class SqlQueryResultListWrapperIterator
#  \brief Iterator class for SQL result wrapper.
class SqlQueryResultListWrapperIterator:
    """Iterator class for SQL result wrapper."""
    
    def __init__(self, list):
        self.list = list
        self.nextIndex = -1
    
    def __iter__(self):
        return self

    def next(self):
        if self.nextIndex + 1 >= len(self.list):
            raise StopIteration
        self.nextIndex = self.nextIndex + 1
        return self.list[self.nextIndex]

    
# =============================================================================
## \class CategoryListWrapper
#  \brief Wrapper for category lists to allow simpler access by column names.
class CategoryListWrapper(SqlQueryResultListWrapper):
    """Wrapper for category lists to allow simpler access by column names."""

    def __init__(self, catList):
        super(CategoryListWrapper, self).__init__(catList)

    def __getitem__(self, key):
        return CategoryWrapper(self.list[key])

    # must match CATEGORY_QUERY_ATTRIBUTES
    def getColumnMaxLength(self, colName):
        if colName == "name":
            colIndex = 0
        elif colName == "code1":
            colIndex = 1
        elif colName == "code2":
            colIndex = 2
        else:
            assert False, "Access to unknown column tried."
        return self.getColumnMaxLengthByIndex(colIndex)
    
# =============================================================================
## \class CategoryWrapper
#  \brief Wrapper for category object which allows member-style access to chosen attributes.
class CategoryWrapper(object):
    """Wrapper for category object which allows member-style access to chosen attributes."""

    def __init__(self, catObj):
        self.catObj = catObj

    # must match CATEGORY_QUERY_ATTRIBUTES
    def __getattr__(self, name):
        if name == "name":
            return self.catObj[0]
        elif name == "code1":
            return self.catObj[1]
        elif name == "code2":
            return self.catObj[2]
        else:
            assert False, "Unknown category attribute in CategoryWrapper class."


# =============================================================================
## \class ProjectListWrapper
#  \brief Wrapper for project lists to allow simpler access by column names.
class ProjectListWrapper(SqlQueryResultListWrapper):
    """Wrapper for project lists to allow simpler access by column names."""

    def __init__(self, projList):
        super(ProjectListWrapper, self).__init__(projList)

    def __getitem__(self, key):
        return ProjectWrapper(self.list[key])

    # must match PROJECT_QUERY_ATTRIBUTES
    def getColumnMaxLength(self, colName):
        if colName == "name":
            colIndex = 0
        elif colName == "description":
            colIndex = 1
        elif colName == "date":
            colIndex = 2
        elif colName == "categoryid":
            colIndex = 3
        elif colName == "categoryname":
            colIndex = 4
        elif colName == "projectcode":
            colIndex = 5
        elif colName == "fullcode":
            colIndex = 6
        else:
            assert False, "Access to unknown column tried."
        return self.getColumnMaxLengthByIndex(colIndex)


# =============================================================================
## \class ProjectWrapper
#  \brief Wrapper for project object which allows member-style access to chosen attributes.
class ProjectWrapper(object):
    """Wrapper for project object which allows member-style access to chosen attributes."""

    def __init__(self, projObj):
        self.projObj = projObj

    # must match PROJECT_QUERY_ATTRIBUTES
    def __getattr__(self, name):
        if name == "name":
            return self.projObj[0]
        elif name == "description":
            return self.projObj[1]
        elif name == "date":
            return self.projObj[2]
        elif name == "categoryid":
            return self.projObj[3]
        elif name == "categoryname":
            return self.projObj[4]
        elif name == "projectcode":
            return self.projObj[5]
        elif name == "fullcode":
            return self.projObj[6]
        else:
            assert False, "Unknown project attribute in ProjectWrapper class."


# =============================================================================
## \class DatabaseQueryHandler
#  \brief Handler for project database queries (with known scheme).
class DatabaseQueryHandler(object):
    """Handler for project database queries (with known scheme)."""

    def __init__(self, filename):
        self.database = ProjectCodeDatabase(filename)
        if self.database.validateDatabase():
            self.valid = True
        else:
            self.valid = False
        self.columnShortnames = { 'n' : "name", 'c1' : "code1", 'c2' : "code2",
                                  'd' : "description", 'dt' : "date", 't' : "date",
                                  'p' : "projectcode", 'pc' : "projectcode",
                                  'f' : "fullcode", 'fc' : "fullcode", 'c' : "fullcode" }
    
    def isValid(self):
        return self.valid

    def getAllCategories(self):
        assert self.valid
        self.database.openDatabase()
        cursor = self.database.queryDatabase("SELECT " + CATEGORY_QUERY_ATTRIBUTES + " FROM categories")
        res = cursor.fetchall()
        self.database.closeDatabase()
        return CategoryListWrapper(res)

    def getAllProjects(self):
        assert self.valid
        self.database.openDatabase()
        cursor = self.database.queryDatabase("SELECT " + PROJECT_QUERY_ATTRIBUTES + " FROM projects JOIN categories ON projects.categoryid == categories.id")
#        cursor = self.database.queryDatabase("SELECT " + PROJECT_QUERY_ATTRIBUTES + " FROM projects, categories WHERE projects.categoryid == categories.id")
        res = cursor.fetchall()
        self.database.closeDatabase()
        return ProjectListWrapper(res)

    def getCategoryByPart(self, value, part=None):
        assert self.valid
        self.database.openDatabase()
        if part:
            cursor = self.database.queryDatabase("SELECT " + CATEGORY_QUERY_ATTRIBUTES + " FROM categories WHERE " + part + " LIKE \"%" + value + "%\"")
        else:
            (part, value) = self.extractPart(value)
            cursor = self.database.queryDatabase("SELECT " + CATEGORY_QUERY_ATTRIBUTES + " FROM categories WHERE name LIKE \"%" + value + "%\" OR code1 LIKE \"%" + value + "%\" OR code2 LIKE \"%" + value + "%\" OR id LIKE \"%" + value + "%\"")
        res = cursor.fetchall()
        self.database.closeDatabase()
        return CategoryListWrapper(res)

    def getProjectByPart(self, value, part=None):
        assert self.valid
        self.database.openDatabase()
        if not part:
            (part, value) = self.extractPart(value)
        if part:
            cursor = self.database.queryDatabase("SELECT " + PROJECT_QUERY_ATTRIBUTES + " FROM projects JOIN categories ON projects.categoryid == categories.id WHERE projects." + part + " LIKE \"%" + value + "%\"")
        else:
            cursor = self.database.queryDatabase("SELECT " + PROJECT_QUERY_ATTRIBUTES + " FROM projects JOIN categories ON projects.categoryid == categories.id WHERE projects.name LIKE \"%" + value + "%\" OR description LIKE \"%" + value + "%\" OR date LIKE \"%" + value + "%\" OR projects.id LIKE \"%" + value + "%\" OR projectcode LIKE \"%" + value + "%\" OR fullcode LIKE \"%" + value + "%\"")
        res = cursor.fetchall()
        self.database.closeDatabase()
        return ProjectListWrapper(res)

    def extractPart(self, value):
        if value.find(':') != -1:
            parts = value.split(':')
            res_part = self.expandColumnShortnames(parts[0])
            res_value = parts[1]
        else:
            res_part = None
            res_value = value
        return (res_part, res_value)

    def expandColumnShortnames(self, short):
        if short in self.columnShortnames.keys():
            return self.columnShortnames[short]
        else:
            return short



    
