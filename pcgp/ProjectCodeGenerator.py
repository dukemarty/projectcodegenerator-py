#! /opt/macports2/bin/python
# This is -*- Python -*- from nbg -*- coding: latin1 -*-
#
##   \file  ProjectCodeGenerator.py
#    \brief Please put your documentation for this file here.
#
#    \par Last Author: Martin Loesch (<martin.loesch@@kit.edu>)
#    \par Date of last change: 16.11.12
#
#    \author   Martin Loesch (professional@@martinloesch.net)
#    \date     2012-08-30
#    \par Copyright:
#              Martin Loesch\n
#	       All rights reserved\n
#              \n
#	       http://re.ve/martin-loesch\n
#	       http://sourceforge.net/users/dukemarty
#

# standard library packages
import os.path
import argparse
# additional external packages
from Crypto.Hash import MD5
# project-related files
from humanreadablemappings import *
from CodePersistencyHandler import *
import config


## \class ProjectCode
#  \brief Representation (and computation) of project codes.
class ProjectCode(object):
    """Representation (and computation) of project codes."""

    
    ## \brief Constructor for ProjectCode objects.
    #
    # @param self self reference of the object
    # @param category name of category the project falls into
    # @param title initial working title of the project
    # @param description initial description of the project (goal, objective(s), ...)
    # @param startdate start date, conception date or similar of the project
    def __init__(self, category="", title="", description="", startdate="",
                 categoryMapping=config.default_category_mapping_file,
                 projectMapping=config.default_project_mapping_file):
        """Constructor for ProjectCode objects."""
        self.category = category
        self.title = title
        self.description = description
        self.startdate = startdate
        if not (os.path.isfile(categoryMapping) and os.path.isfile(projectMapping)):
            assert False, "Phonetic mapping files do not exists!"
        self.categoryMapping = StringToPhoneticMapping(categoryMapping)
        self.projectMapping = StringToPhoneticMapping(projectMapping)
        self.computeProjectcode()

    ## \brief Set category of the project.
    #
    # @param self self reference of the object
    # @param category new category of the project
    def setCategory(self, category):
        """Set category of the project."""
        self.category = category
        self.computeProjectcode()

    def setTitle(self, title):
        self.title = title
        self.computeProjectcode()

    def setDescription(self, description):
        self.description = description
        self.computeProjectcode()

    def setStartDate(self, startdate):
        self.startdate = startdate
        self.computeProjectcode()

    def computeProjectcode(self):
        self.computeProjectHashes()
        self.computeCodeParts()

    def computeProjectHashes(self):
        h = MD5.new(self.category)
        self.categoryHash = h.digest()
        h = MD5.new(self.title + self.description + self.startdate)
        self.projectHash = h.digest()

    def computeCodeParts(self):
        self.categoryCode = []
        self.categoryCode.append(mapNumberToChar(ord(self.categoryHash[0])*256 + ord(self.categoryHash[1])).upper())
        self.categoryCode.append(mapAnysymbolToLowerAlphaNumOrSpace(self.categoryHash[0]).upper() + " " + mapSymbolToDigit(self.categoryHash[1]))
        pcParts = []
        pcParts.append(mapAnysymbolToLowerAlphaNumOrSpace(self.projectHash[0]).upper())
        pcParts.append(mapBinstringToNumber(self.projectHash[1:5]))
        pcParts.append("[" + mapBinstringToNumber(self.projectHash[5:9]) + "]")
        self.projectCode = reduce(lambda x,y: x + " " + y, pcParts)

    def getCategoryCodes(self):
        return self.categoryCode

    def getProjectCode(self):
        return self.projectCode

    def constructPhoneticCode(self, catMapping, projMapping, showvariant):
        if showvariant == "all":
            catcode = "{ " + reduce(lambda x,y: x + " / " + y, map(lambda x: "".join(catMapping.stringToPhoneticNames(x)), self.getCategoryCodes())) + " }"
        else:
            catcode = "".join(catMapping.stringToPhoneticNames(self.getCategoryCodes()[int(showvariant)-1]))
        
        return catcode + " " + "".join(projMapping.stringToPhoneticNames(self.getProjectCode()))
        

def printCodes(args, projectcode, catMapping, projMapping):
    if args.showcatcode == "all":
        catcode = "{ " + reduce(lambda x,y: x + " / " + y, map(lambda x: "".join(catMapping.stringToPhoneticNames(x)), projectcode.getCategoryCodes())) + " }"
    else:
        catcode = "".join(catMapping.stringToPhoneticNames(projectcode.getCategoryCodes()[int(args.showcatcode)-1]))
    
    print "Plain data:"
    print "  Category:    ", args.category
    if args.projectname:
        print "  Name:        ", args.projectname
    if args.projectdescription:
        print "  Description: ", args.projectdescription
    if args.projectdate:
        print "  Start date:  ", args.projectdate
    print "-------------------------------------------------"
    print "Encoded data:"
    print "  Category code:  ", catcode
    print "  Project code:   ", "".join(projMapping.stringToPhoneticNames(projectcode.getProjectCode()))
    print " - - - - - - - - - - - - - - - - - - - -"
    print "  Complete code:  ", projectcode.constructPhoneticCode(catMapping, projMapping, args.showcatcode)
    print ""


def storeCodes(args, projectcode, catMapping, projMapping):
    # check file for existence and validity/usableness

    db = ProjectCodeDatabase(args.store)
    if not db.validateOrInitDatabase():
        return False
    
    if args.projectname=="" and args.projectdescription=="" and args.projectdate=="":
        categoryCodes = projectcode.getCategoryCodes()
        db.addCategory(args.category, categoryCodes[0], categoryCodes[1])
    else:
        catId = db.findCategory(args.category)
        db.addProject(projectcode.title, projectcode.description, projectcode.startdate, catId[0], projectcode.getProjectCode(), projectcode.constructPhoneticCode(catMapping, projMapping))
        
    return True

def loadCategoryNameFromDatabase(store, category):
    db = ProjectCodeDatabase(store)
    if not db.validateOrInitDatabase():
        return False
    catId = db.findCategory(category)
    if catId:
        return catId[1]
    else:
        return category
    
## TRUE MAIN PROGRAM
#
## \cond false
import sys

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Compute deterministic project codes.", epilog="The storage into an sqlite3 database of a project is only possible if the related category already exists in it. To do this, the program has to be called first with the store option, but only a category as parameter.")
    parser.add_argument('category', metavar='CAT', help="Category of the project, e.g. research")
    parser.add_argument('projectname', metavar='PROJ', nargs='?', default="", help="Initial (speaking) name of the project to be coded")
    parser.add_argument('projectdescription', metavar='DESCR', nargs='?', default="", help="Initial description of the project")
    parser.add_argument('projectdate', metavar='DATE', nargs='?', default="", help="Start or conception date or similar of the project")
    parser.add_argument('-s', '--store', metavar='DB', help="Store (sqlite 3 database file) resulting codes are written to")
    parser.add_argument('-cm', '--catmap', metavar='CM', default=config.default_category_mapping_file, help="Phonetic mapping for category code")
    parser.add_argument('-pm', '--projmap', metavar='PM', default=config.default_project_mapping_file, help="Phonetic mapping for project code")
    parser.add_argument('-scc', '--showcatcode', metavar='SCC', default=config.default_used_category_code, choices=['1', '2', 'all'], help="Select which category code to show (1 or two chars or both)")
    
    args = parser.parse_args()

    print "      ** ProjectCodeGenerator **\n"

    if args.store and not (args.projectname=="" and args.projectdescription=="" and args.projectdate==""):
        args.category = loadCategoryNameFromDatabase(args.store, args.category)
    
    pc = ProjectCode(args.category, args.projectname, args.projectdescription, args.projectdate)
    catMapping = StringToPhoneticMapping(config.default_category_mapping_file)
    projMapping = StringToPhoneticMapping(config.default_project_mapping_file)
    
    printCodes(args, pc, catMapping, projMapping)

    if args.store:
        storeCodes(args, pc, catMapping, projMapping)
    
## \endcond
    
