#! /opt/macports2/bin/python
# This is -*- Python -*- from nbg -*- coding: latin1 -*-
#
##   \file  pcgcommandlineinterface.py
#    \brief Please put your documentation for this file here.
#
#    \par Last Author: Martin Loesch (<martin.loesch@@kit.edu>)
#    \par Date of last change: 12.11.12
#
#    \author   Martin Loesch (professional@@martinloesch.net)
#    \date     2012-09-06
#    \par Copyright:
#              Martin Loesch\n
#	       All rights reserved\n
#              \n
#	       http://re.ve/martin-loesch\n
#	       http://sourceforge.net/users/dukemarty
#

import argparse
from ProjectCodeGenerator import *


def printHeader():
    print " ** ProjectCodeGenerator - CLI **\n"

    
def getDataFromUser():
    category = ""
    while category == "":
        category = raw_input("Project category: ")
    projname = ""
    while projname == "":
        projname = raw_input("Project name:     ")

    projdescription = raw_input("Project description:  ")
    projdate = raw_input("Start date:  ")

    return (category, projname, projdescription, projdate)


def printGivenData(cat, projname, projdescr, projdate):
    print "================================================="
    print "Plain data:"
    print "  Category:    ", cat
    print "  Name:        ", projname
    print "  Description: ", projdescr
    print "  Start date:  ", projdate
    print ""
    print "================================================="
    print ""


def printResults(pc):
    print "================================================="
    print "Encoded data:"
    print "  Category code:  ", pc.getCategoryCodes()
    print "  Project code:   ", pc.getProjectCode()
    print " - - - - - - - - - - - - - - - - - - - -"
#    print "  Complete code:  ", pc.constructPhoneticCode()
    print "================================================="

    

## TRUE MAIN PROGRAM
#
## \cond false
import sys

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Interactive command line interface for the ProjectCodeGenerator.", epilog="After starting this program, it will ask for project category, project name and description and start date of the project for which a code shall be generated. Only category and name are obligatory, a description and start date are optional.")
    args = parser.parse_args()

    printHeader()

    (category, projname, projdescription, projdate) = getDataFromUser()
    printGivenData(category, projname, projdescription, projdate)
    
    pc = ProjectCode(category, projname, projdescription, projdate)
    printResults(pc)
    
    
## \endcond
    
