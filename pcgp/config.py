#! /opt/macports2/bin/python
# This is -*- Python -*- from nbg -*- coding: latin1 -*-
#
##   \file  config.py
#    \brief This file contains general configuration settings, mainly
#           pathes and some user-chosen constants.
#
#    \par Last Author: Martin Loesch (<professional@@martinloesch.net>)
#    \par Date of last change: 10.09.13
#
#    \author   Martin Loesch (professional@@martinloesch.net)
#    \date     2012-09-16
#    \par Copyright:
#              Martin Loesch\n
#	       All rights reserved\n
#              \n
#	       http://re.ve/martin-loesch\n
#	       http://sourceforge.net/users/dukemarty
#


# mapping files from character to word for category and project codes
default_category_mapping_file = "../data/GreekInspiredAlphabet.txt"
default_project_mapping_file = "../data/NATOPhoneticAlphabet.txt"

# value how much characters for category codes are used - 1 and 2 are valid
default_used_category_code = "2"
