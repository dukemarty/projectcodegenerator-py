#! /opt/macports2/bin/python
# This is -*- Python -*- from nbg -*- coding: latin1 -*-
#
##   \file  test_humanreadablemappings.py
#    \brief Please put your documentation for this file here.
#
#    \par Last Author: Martin Loesch (<professional@@martinloesch.net>)
#    \par Date of last change: 06.11.12
#
#    \author   Martin Loesch (professional@@martinloesch.net)
#    \date     2012-10-20
#    \par Copyright:
#              Martin Loesch\n
#	       All rights reserved\n
#              \n
#	       http://re.ve/martin-loesch\n
#	       http://sourceforge.net/users/dukemarty
#

import unittest
import humanreadablemappings



class TestMapNumberToChar(unittest.TestCase):
    
    def test_paramsWithin26(self):
        self.assertEqual(humanreadablemappings.mapNumberToChar(0), 'a')
        self.assertEqual(humanreadablemappings.mapNumberToChar(1), 'b')
        self.assertEqual(humanreadablemappings.mapNumberToChar(26), 'a')

    def test_paramsBeyond26(self):
        self.assertEqual(humanreadablemappings.mapNumberToChar(27), 'b')
        self.assertEqual(humanreadablemappings.mapNumberToChar(-1), 'z')

        
class TestMapSymbolToChar(unittest.TestCase):
        
    def test_alphaChars(self):
        self.assertEqual(humanreadablemappings.mapSymbolToChar('a'), 'a')
        self.assertEqual(humanreadablemappings.mapSymbolToChar('g'), 'g')
        self.assertEqual(humanreadablemappings.mapSymbolToChar('z'), 'z')
        self.assertEqual(humanreadablemappings.mapSymbolToChar('A'), 'A')
        self.assertEqual(humanreadablemappings.mapSymbolToChar('G'), 'G')
        self.assertEqual(humanreadablemappings.mapSymbolToChar('Z'), 'Z')

    def test_digits(self):
        self.assertEqual(humanreadablemappings.mapSymbolToChar('0'), '0')
        self.assertEqual(humanreadablemappings.mapSymbolToChar('1'), '1')
        self.assertEqual(humanreadablemappings.mapSymbolToChar('9'), '9')

    def test_spaces(self):
        self.assertEqual(humanreadablemappings.mapSymbolToChar(' '), ' ')
        self.assertEqual(humanreadablemappings.mapSymbolToChar('\t'), '\t')
        self.assertEqual(humanreadablemappings.mapSymbolToChar('\n'), '\n')

    def test_arbitraryChar(self):
        self.assertTrue(humanreadablemappings.mapSymbolToChar(chr(0)).isalpha())
        self.assertTrue(humanreadablemappings.mapSymbolToChar(chr(42)).isalpha())
        self.assertTrue(humanreadablemappings.mapSymbolToChar(chr(77)).isalpha())
        self.assertTrue(humanreadablemappings.mapSymbolToChar(chr(133)).isalpha())
        self.assertTrue(humanreadablemappings.mapSymbolToChar(chr(200)).isalpha())
        self.assertTrue(humanreadablemappings.mapSymbolToChar(chr(235)).isalpha())
        
        
class TestMapSymbolToDigit(unittest.TestCase):

    def test_digitsStay(self):
        self.assertEqual(humanreadablemappings.mapSymbolToDigit('0'), '0');
        self.assertEqual(humanreadablemappings.mapSymbolToDigit('1'), '1');
        self.assertEqual(humanreadablemappings.mapSymbolToDigit('9'), '9');

    def test_nondigitsBecomeDigits(self):
        self.assertTrue(humanreadablemappings.mapSymbolToDigit(chr(0)).isdigit())
        self.assertTrue(humanreadablemappings.mapSymbolToDigit(chr(13)).isdigit())
        self.assertTrue(humanreadablemappings.mapSymbolToDigit(chr(240)).isdigit())
        self.assertTrue(humanreadablemappings.mapSymbolToDigit(chr(255)).isdigit())


class TestMapAnySymbolToLowerAlphaNumOrSpace(unittest.TestCase):

    def test_alphaChars(self):
        self.assertEqual(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace('a'), 'a')
        self.assertEqual(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace('A'), 'a')
        self.assertEqual(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace('h'), 'h')
        self.assertEqual(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace('H'), 'h')
        self.assertEqual(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace('z'), 'z')
        self.assertEqual(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace('Z'), 'z')

    def test_digitsSpaces(self):
        self.assertEqual(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace('1'), '1')
        self.assertEqual(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace('9'), '9')
        self.assertEqual(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace('0'), '0')
        self.assertEqual(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace(' '), ' ')
        self.assertEqual(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace('\t'), '\t')
        self.assertEqual(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace('\n'), '\n')
        
    def test_nonAlphaPrintables(self):
        self.assertTrue(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace('#').isalpha())
        self.assertTrue(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace('.').isalpha())

    def test_nonAlphaArbitrary(self):
        self.assertTrue(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace(chr(0)).isalpha())
        self.assertTrue(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace(chr(255)).isalpha())
        self.assertTrue(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace(chr(14)).isalpha())
        self.assertTrue(humanreadablemappings.mapAnysymbolToLowerAlphaNumOrSpace(chr(169)).isalpha())


class TestMapBinstringToChars(unittest.TestCase):

    def setUp(self):
        self.teststring1 = "15aab6238f4c3deda5f8984b56".decode('hex')
    
    def test_arbitraryhexstring(self):
        mappedstring1 = humanreadablemappings.mapBinstringToChars(self.teststring1)
        for c in mappedstring1:
            self.assertTrue(c.isalpha())


class TestMapBinstringToNumber(unittest.TestCase):

    def setUp(self):
        self.teststring1 = "15aab6238f4c3deda5f8984b56".decode('hex')

    def test_arbitraryhexstring(self):
        mappedstring1 = humanreadablemappings.mapBinstringToNumber(self.teststring1)
        for c in mappedstring1:
            self.assertTrue(c.isdigit())

        
        
## TRUE MAIN PROGRAM
#
## \cond False
if __name__ == '__main__':
    print "Running unit tests for  humanreadablemappings.py\n"
    suitelist = []
    suitelist.append(unittest.TestLoader().loadTestsFromTestCase(TestMapNumberToChar))
    suitelist.append(unittest.TestLoader().loadTestsFromTestCase(TestMapSymbolToChar))
    suitelist.append(unittest.TestLoader().loadTestsFromTestCase(TestMapSymbolToDigit))
    suitelist.append(unittest.TestLoader().loadTestsFromTestCase(TestMapAnySymbolToLowerAlphaNumOrSpace))
    suitelist.append(unittest.TestLoader().loadTestsFromTestCase(TestMapBinstringToChars))
    suitelist.append(unittest.TestLoader().loadTestsFromTestCase(TestMapBinstringToNumber))
    alltests_suite = unittest.TestSuite(suitelist)
    unittest.TextTestRunner(verbosity=2).run(alltests_suite)

## \endcond
    
