#! /opt/macports2/bin/python
# This is -*- Python -*- from nbg -*- coding: latin1 -*-
#
##   \file  all_tests.py
#    \brief Please put your documentation for this file here.
#
#    \par Last Author: Martin Loesch (<professional@@martinloesch.net>)
#    \par Date of last change: 06.11.12
#
#    \author   Martin Loesch (professional@@martinloesch.net)
#    \date     2012-11-01
#    \par Copyright:
#              Martin Loesch\n
#	       All rights reserved\n
#              \n
#	       http://re.ve/martin-loesch\n
#	       http://sourceforge.net/users/dukemarty
#

import unittest


## TRUE MAIN PROGRAM
#
## \cond false
if __name__ == '__main__':
    print "Running all unit tests for PCGP\n"

    unittest.TextTestRunner(verbosity=2).run(unittest.defaultTestLoader.discover("."))

## \endcond
    
