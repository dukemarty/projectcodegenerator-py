#! /opt/macports2/bin/python
# This is -*- Python -*- from nbg -*- coding: latin1 -*-
#
##   \file  test_CodePersistencyHandler.py
#    \brief Please put your documentation for this file here.
#
#    \par Last Author: Martin Loesch (<professional@@martinloesch.net>)
#    \par Date of last change: 06.11.12
#
#    \author   Martin Loesch (professional@@martinloesch.net)
#    \date     2012-10-28
#    \par Copyright:
#              Martin Loesch\n
#	       All rights reserved\n
#              \n
#	       http://re.ve/martin-loesch\n
#	       http://sourceforge.net/users/dukemarty
#

import unittest
import os, tempfile
import CodePersistencyHandler



class TestProjectCodeDatabase(unittest.TestCase):

    def setUp(self):
        tempfile.tempdir = "tempdata/"
        (tmpf, self.tempfilename1) = tempfile.mkstemp()
        os.close(tmpf)
        (tmpf, self.tempfilename2) = tempfile.mkstemp()
        os.close(tmpf)
        (tmpf, self.tempfilename3) = tempfile.mkstemp()
        os.close(tmpf)

    def tearDown(self):
        os.remove(self.tempfilename1)
        os.remove(self.tempfilename2)
        os.remove(self.tempfilename3)
        
    def test_Constructor(self):
        testdb = CodePersistencyHandler.ProjectCodeDatabase("bla")
        self.assertEqual(testdb.filename, "bla")

    def test_initEmptyDatabaseOnExistingFile(self):
        testdb = CodePersistencyHandler.ProjectCodeDatabase(self.tempfilename1)
        self.assertRaises(AssertionError, testdb.initEmptyDatabase)

    def test_initEmptyDatabaseOnNewFile(self):
        os.remove(self.tempfilename1)
        testdb = CodePersistencyHandler.ProjectCodeDatabase(self.tempfilename1)
        self.assertTrue(testdb.initEmptyDatabase())
        
    def test_validateDatabase(self):
        # existing empty file
        testdb1 = CodePersistencyHandler.ProjectCodeDatabase(self.tempfilename1)
        self.assertFalse(testdb1.validateDatabase())
        # newly initialized database
        os.remove(self.tempfilename2)
        testdb2 = CodePersistencyHandler.ProjectCodeDatabase(self.tempfilename2)
        testdb2.initEmptyDatabase()
        self.assertTrue(testdb2.validateDatabase())

    def test_validOrInitDatabase(self):
        # non-existent file
        os.remove(self.tempfilename1)
        testdb1 = CodePersistencyHandler.ProjectCodeDatabase(self.tempfilename1)
        self.assertTrue(testdb1.validateOrInitDatabase())
        # existing empty file
        testdb2 = CodePersistencyHandler.ProjectCodeDatabase(self.tempfilename2)
        self.assertFalse(testdb2.validateOrInitDatabase())
        # existing database
        os.remove(self.tempfilename3)
        testdb3 = CodePersistencyHandler.ProjectCodeDatabase(self.tempfilename3)
        testdb3.initEmptyDatabase()
        self.assertTrue(testdb3.validateOrInitDatabase())
        
        
        
## TRUE MAIN PROGRAM
#
## \cond false
if __name__ == '__main__':
    print "Running unit tests for CodePersistencyHandler.py\n"
    suitelist = []
    suitelist.append(unittest.TestLoader().loadTestsFromTestCase(TestProjectCodeDatabase))
    alltests_suite = unittest.TestSuite(suitelist)
    unittest.TextTestRunner(verbosity=2).run(alltests_suite)

## \endcond
    
