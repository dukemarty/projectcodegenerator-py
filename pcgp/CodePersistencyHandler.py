#! /opt/macports2/bin/python
# This is -*- Python -*- from nbg -*- coding: latin1 -*-
#
##   \file  CodePersistencyHandler.py
#    \brief Please put your documentation for this file here.
#
#    \par Last Author: Martin Loesch (<martin.loesch@@kit.edu>)
#    \par Date of last change: 13.11.12
#
#    \author   Martin Loesch (professional@@martinloesch.net)
#    \date     2012-09-11
#    \par Copyright:
#              Martin Loesch\n
#	       All rights reserved\n
#              \n
#	       http://re.ve/martin-loesch\n
#	       http://sourceforge.net/users/dukemarty
#

import sqlite3
import sys, os


class DatabaseError(IOError):

    def __init__(self, args):
        super(DatabaseError, self).__init__(args)
        
    
    
class ProjectCodeDatabase(object):

    def __init__(self, filename):
        self.filename = filename
        self.conn = None
        self.cursor = None
        self.columnShortnames = { 'n' : "name", 'c1' : "code1", 'c2' : "code2" }

    def validateOrInitDatabase(self):
        if os.path.isfile(self.filename):
            return self.validateDatabase()
        else:
            return self.initEmptyDatabase()

    def validateDatabase(self):
        assert os.path.isfile(self.filename)
        conn = sqlite3.connect(self.filename)
        c = conn.cursor()
        try:
            c.execute(''' SELECT id, name, code1, code2 FROM categories WHERE id<10 ''')
            c.execute(''' SELECT id, name, description, date, categoryid, projectcode, fullcode FROM projects WHERE id<10 ''')
        except sqlite3.OperationalError as e:
            if e.args[0].find("no such column")>-1:
                sys.stderr.write("Required column missing in table:  " + e.args[0] + "\n")
                return False
            elif e.args[0].find("no such table")>-1:
                sys.stderr.write("Required table missing in database:  " + e.args[0] + "\n")
                return False
        c.close()
        return True

    def initEmptyDatabase(self):
        assert not os.path.isfile(self.filename)
        conn = sqlite3.connect(self.filename)
        c = conn.cursor()
        c.execute('''CREATE TABLE categories (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL UNIQUE, code1 TEXT, code2 TEXT)''')
        c.execute('''CREATE TABLE projects (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, description TEXT, date TEXT, categoryid INTEGER REFERENCES categories (id), projectcode TEXT, fullcode TEXT)''')
        conn.commit()
        c.close()
        conn.close()
        return True
            
    def openDatabase(self):
        self.conn = sqlite3.connect(self.filename)
        self.cursor = self.conn.cursor()

    def closeDatabase(self, commitChanges=True):
        if commitChanges:
            self.conn.commit()
        self.cursor.close()
        self.cursor = None
        self.conn.close()
        self.conn = None

    def findCategory(self, category):
        if category.find(':') != -1:
            parts = category.split(':')
            col = self.expandColumnShortnames(parts[0])
            val = parts[1]
            self.openDatabase()
            cursor = self.queryDatabase("SELECT id, name FROM categories WHERE " + col + " LIKE \"%" + val + "%\"")
            res = cursor.fetchall()
            self.closeDatabase()
        else:
            where_clause_name = "(name LIKE \"%" + category + "%\")"
            where_clause_code1 = "(code1 LIKE \"%" + category + "%\")"
            where_clause_code2 = "(code2 LIKE \"%" + category + "%\")"
            where_clause_id = "(id LIKE \"%" + category + "%\")"
            full_where_clause = where_clause_name + " OR " + where_clause_code1 + " OR " + where_clause_code2 + " OR " + where_clause_id
            self.openDatabase()
            cursor = self.queryDatabase("SELECT id, name FROM categories WHERE ( " + full_where_clause + " )")
            res = cursor.fetchall()
            self.closeDatabase()
        if len(res) == 0:
            sys.stderr.write("Error: Category could not be found!\n")
            return None
        elif len(res) == 1:
            return res[0]
        else:
            sys.stderr.write("Error: Found more than one category matching the given description!\n")
            return res[0]
        
    def queryDatabase(self, query):
        assert self.conn and self.cursor
        self.cursor.execute(query)
        return self.cursor

    def expandColumnShortnames(self, short):
        if short in self.columnShortnames.keys():
            return self.columnShortnames[short]
        else:
            return short
    
    ## \brief Add new category to database.
    #
    # The methods throws a DatabaseError if any unexpected error occurs.
    #
    # @param self self reference object
    # @param categoryname name of new category
    # @param categorycode1 code 1 for category
    # @param categorycode2 code 2 for category
    # @return False denotes an error state, True the successful writing to the database
    def addCategory(self, categoryname, categorycode1, categorycode2):
        """Add category to project code database."""
        self.openDatabase()
        try:
            self.cursor.execute('''INSERT INTO categories (name, code1, code2) VALUES (?, ?, ?)''', (categoryname, categorycode1, categorycode2) )
        except sqlite3.IntegrityError as e:
            sys.stderr.write("Error when adding category to database:  " + e.args[0] + "\n")
            return False
        except:
            sys.stderr.write("Encountered unexpected error when adding category to database:  " + sys.exc_info()[0] + "\n")
            raise DatabaseError("Encountered unexpected error when adding category to database:  " + sys.exc_info()[0])
        self.closeDatabase()
        return True

    
    ## \brief Add new project to database.
    #
    # The methods throws a DatabaseError if any unexpected error occurs.
    #
    # @param self self reference object
    # @param projectname
    # @param projectdescription
    # @param projectdate
    # @param categoryid
    # @param projectcode
    # @param fullcode
    # @return False denotes an error state, True the successful writing to the database
    def addProject(self, projectname, projectdescription, projectdate, categoryid, projectcode, fullcode):
        self.openDatabase()
        try:
            self.cursor.execute('''INSERT INTO projects (name, description, date, categoryid, projectcode, fullcode) VALUES(?, ?, ?, ?, ?, ?)''', (projectname, projectdescription, projectdate, categoryid, projectcode, fullcode) )
        except sqlite3.IntegrityError as e:
            sys.stderr.write("Error when adding project to database:  " + e.args[0] + "\n")
            return False
        except:
            sys.stderr.write("Encountered unexpected error when adding category to database:  " + sys.exc_info()[0] + "\n")
            raise DatabaseError("Encountered unexpected error when adding category to database:  " + sys.exc_info()[0])
        self.closeDatabase()
        return True
    
