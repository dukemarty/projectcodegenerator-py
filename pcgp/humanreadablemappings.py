#! /opt/macports2/bin/python
# This is -*- Python -*- from nbg -*- coding: latin1 -*-
#
##   \file  humanreadablemappings.py
#    \brief Map characters (bytes) and other types to a human readable/pronouncable format (not necessarily reversible).
#
#    \par Last Author: Martin Loesch (<professional@@martinloesch.net>)
#    \par Date of last change: 07.11.12
#
#    \author   Martin Loesch (professional@@martinloesch.net)
#    \date     2012-08-30
#    \par Copyright:
#              Martin Loesch\n
#	       All rights reserved\n
#              \n
#	       http://re.ve/martin-loesch\n
#	       http://sourceforge.net/users/dukemarty
#

import argparse

class StringToPhoneticMapping(object):
    """Class which represents (and applies) phonetic mappings for strings."""
    
    def __init__(self, filename=None):
        """Constructor."""
        self.mapping = {}
        if filename!=None:
            self.load(filename)

    def load(self, filename):
        """Load phonetic mapping from file."""
        mapfile = open(filename, 'r')
        lines = mapfile.readlines()
        for l in lines:
            parts = l.split(',')
            if len(parts) == 2:
                self.mapping[parts[0].strip()] = parts[1].strip()

    def printMapping(self):
        """Output mapping pairs to standard out."""
        print self.mapping

    def alphacharToPhoneticName(self, singlechar):
        """Map single character to phonetic representation."""
        if singlechar.lower() in self.mapping.keys():
            return self.mapping[singlechar.lower()]
        elif singlechar.isdigit() or singlechar.isspace():
            return singlechar
        else:
            return singlechar

    def anysymbolToPhoneticName(self, singlechar):
        sym = mapAnysymbolToLowerAlphaNumOrSpace(singlechar)
        return self.mapping[sym]
        
    def stringToPhoneticNames(self, multiplechars):
        """Map a string of multiple characters to array of phonetic representations."""
        return map(self.alphacharToPhoneticName, multiplechars)

def mapNumberToChar(number):
    return chr(ord('a') + (number % 26))

def mapSymbolToChar(singlechar):
    if singlechar.isalnum() or singlechar.isspace():
        return singlechar
    else:
        return chr(ord('a') + (ord(singlechar) % 26))

def mapSymbolToDigit(singlechar):
    if singlechar.isdigit():
        return singlechar
    else:
        return str(ord(singlechar)%10)

def mapAnysymbolToLowerAlphaNumOrSpace(singlechar):
    if singlechar.lower().isalpha():
        return singlechar.lower()
    else:
        return mapSymbolToChar(singlechar)
    
def mapBinstringToChars(fullstring):
    return "".join(map(mapSymbolToChar, fullstring))

def mapBinstringToNumber(fullstring):
    return "".join(map(mapSymbolToDigit, fullstring))


## TRUE MAIN PROGRAM
#
## \cond false
import sys
    
def main():
    parser = argparse.ArgumentParser(description="Map arbitrary codes to human readable symbols.", epilog="Description of the available commands: (translate): Take a string in hex representation and transform it so that the resulting string contains only alphanumeric characters.  (makereadable): Transform a string of alphanumeric characters into a series of phonetic descriptions of the original string (digits are left untouched).")
    parser.add_argument('command', metavar='CMD', help="Action to perform with the given symbol sequence [translate, makereadable].")
    parser.add_argument('symbols', metavar='SYMBOLS', help="Symbols which shall be transcribed (list of characters for translate, hex code for makereadable).")

    args = parser.parse_args()
    
    print " ** Human Readable Mapping **\n"

    if args.command == "translate":
        greek_mapper = StringToPhoneticMapping("../data/GreekInspiredAlphabet.txt")
        nato_mapper = StringToPhoneticMapping("../data/NATOPhoneticAlphabet.txt")
        print reduce(lambda x,y: x + " " + y, filter(None, greek_mapper.stringToPhoneticNames(args.symbols)))
        print reduce(lambda x,y: x + " " + y, filter(None, nato_mapper.stringToPhoneticNames(args.symbols)))
    elif args.command == "makereadable":
        print reduce(lambda x,y: x + y, mapBinstringToChars(args.symbols.decode('hex')))


if __name__ == '__main__':
    main()
        
## \endcond
    
